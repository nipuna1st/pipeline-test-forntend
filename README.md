# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Run frontend in local ###
1. npm install
2. npm start

### Run using Docker ###
1. Execute the following command to build the docker image
    <br><b>docker build -t compose-frontend .</b>
1. Execute following to run the docker image
    <br><b>docker run -it -p 3000:3000 compose-frontend</b>