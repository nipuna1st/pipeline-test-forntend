import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Organizer from './containers/Organizer/Organizer'
import FormBuilder from './containers/FormBuilder/FormBuilder'
import Layout from './components/Layout/Layout'

function App() {
  return (
    <BrowserRouter >
      <Layout>
        <Switch>
          <Route path="/form-builder" component={FormBuilder} />
          <Route exact path="/" component={Organizer} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
