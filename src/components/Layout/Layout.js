import React from 'react'
import SideBar from '../Navigation/SideBar/SideBar';
import TopBar from '../Navigation/TopBar/TopBar';

const Layout = (props) => {
    return (
        <React.Fragment>
            <div className="auto-row-2">
                <div className="cp-right-pannel-wrapper">
                    <TopBar />
                    <div className="tv-right-content">
                        <div style={{ display: 'grid', gridTemplateColumns: '2fr 10fr' }}>
                            <SideBar />
                            <div >
                                {props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Layout
