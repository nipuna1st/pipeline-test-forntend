import React from 'react'
import { NavLink } from 'react-router-dom';

const setNavLinkIsActive = (match, location) => match;
const SideBar = props => {
    return (
        <div className="cp-left-pannel__wrapper">
            <div className="cp-left-content">
                <nav className="cp-left-pannel__sidebar">
                    <NavLink isActive={setNavLinkIsActive} to='' className="cp-left-pannel__sidebar-item">Organizer</NavLink>
                    <NavLink to='/form-builder/333' className="cp-left-pannel__sidebar-item">Forms</NavLink>
                </nav>
            </div>
        </div>
    )
}


export default SideBar
