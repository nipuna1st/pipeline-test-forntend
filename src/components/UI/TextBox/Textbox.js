import React from 'react'
import PropTypes from 'prop-types'

const propTypes = {
    properties:PropTypes.shape(
        { }
    )
  };
  
  const defaultProps = {
    properties:{
        labelName:'label',
        labelPosition:'row',
        labelStyle:{width:'50%'},
        controllerStyle:{width:'50%'},
        placeholder: {}
    }
  };

const ComposeTextbox = ({properties,key}) => {
    return (
        <div id={key} style={{display: 'flex', flexDirection:properties.labelPosition}}>
            <label style={{...properties.labelStyle}}>{properties.labelName}</label>
            <input placeholder={properties.placeholder} type="text" style={{...properties.controllerStyle}}/>
        </div>
    )
}

ComposeTextbox.propTypes = propTypes;
ComposeTextbox.defaultProps = defaultProps;

export default ComposeTextbox
