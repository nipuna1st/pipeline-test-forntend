import React, { useState } from 'react'
import ComposeTextbox from '../../components/UI/TextBox/Textbox';

const FormBuilder = props => {
  const [controller, setController] = useState({
    properties: {
      labelName: 'label',
      labelPosition: 'row',
      labelStyle: { width: '50%' },
      controllerStyle: { width: '50%' },
      placeholder: {}
    }
  });

  return (
    <div style={{ display: 'grid', gridTemplateColumns: '5fr 5fr', marginTop: '5%' }}>

      <div style={{ width: '80%', height: '30vh', boxShadow: '0 0 12px 0 #dfdfdf80', padding: '25px', backgroundColor: 'white' }}>

        <ComposeTextbox properties={controller.properties} />
      </div>
      <div style={{ width: '80%', height: '70vh', boxShadow: '0 0 12px 0 #dfdfdf80', padding: '25px', backgroundColor: 'white' }}>
        <h4 style={{ height: '25px' }}>Properties</h4>
        <div className="auto-col-2" style={{ height: '25px' }}>
          <label for="labelPosition">Label Position:</label>
          <select name="labelPosition" id="labelPosition" onChange={(e) => { setController({ properties: { ...controller.properties, labelPosition: e.target.value } }) }}>
            <option value='row'>Left</option>
            <option value='column'>Top</option>
          </select>
        </div>
        <div className="auto-col-2" style={{ height: '25px' }}>
          <label for="labelName">Label Name:</label>
          <input type="text" name="labelName" id="labelName" value={controller.properties.labelName} onChange={(e) => { setController({ properties: { ...controller.properties, labelName: e.target.value } }) }} />
        </div>
      </div>
    </div>
  )
}

export default FormBuilder
